" Configuration file for NeoVim
" Author: Javier de Marco

source ~/.config/nvim/base.vim
source ~/.config/nvim/plugins.vim
source ~/.config/nvim/mappings.vim
source ~/.config/nvim/coc.vim

