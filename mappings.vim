" Keymapping for NeoVim
"
" Author: Javier de Marco
" Key Mappings
"Lazy Mappings
inoremap jk <esc>
tnoremap <Esc> <C-\><C-n>
nmap <leader>h :noh<CR>
" Navegation keys
nmap <C-J> <C-W>j
nmap <C-K> <C-W>k
nmap <C-L> <C-W>l
nmap <C-H> <C-W>h
nmap <A-j> jzz
nmap <A-k> kzz
" Buffers
nmap <leader><Tab> :bn<CR>
nmap <leader><S-Tab> :bp<CR>
" Window Resize
nmap <C-+> <C-W>>
nmap <C-_> <C-W><
nmap <C-Up> :res -3<CR>
nmap <C-Down> :res +3<CR>
" Close buffer
nmap <leader>b :bd<CR>
" Which key
nmap <silent> <leader> :<c-u>WhichKey '<Space>'<CR>
vnoremap <silent> <leader> :<c-u>WhichKeyVisual '<Space>'<CR>
" Define prefix dictionary
let g:which_key_map = {}
" Window related
let g:which_key_map.w = {
    \ 'name' : 'Windows',
    \ 'n' : ['<C-W>n', 'New Window']
    \ }

 " Close
let g:which_key_map.w.c = {
    \ 'name' : 'Close'					,
    \ 'o' : ['<C-W>o',	'All Except Open']	,
    \ 'c' : ['<C-W>c',	'Focused Window']	,
    \ }
 " Split
let g:which_key_map.w.s = {
    \ 'name' : 'Split'					,
    \ 'v' : [':vs',	'Vertial']		,
    \ 'h' : [':sp',	'Horizontal']	,
    \ }
" Tabs
let g:which_key_map.t = {
    \ 'name' : 'Tabs'   ,
    \ 'e' : [':Tex', 'Tab and explore']    ,
    \ 't' : [':tabnew', 'New Tab'],
    \ 'c' : [':tabclse', 'Close Tab'],
    \ 'n' : [':tabn', 'Next Tab'],
    \ 'p' : [':tabp', 'Previous Tab']
    \ }
" Fuzzy Finder related
let g:which_key_map.z = {
    \ 'name' : 'FuzzyFinder'				,
    \ 'f' : [':Telescope find_files',	'Fuzzy Files']		,
    \ 'b' : [':Telescope buffers',	'Fuzzy Buffers']	,
    \ 'g' : [':Telescope live_grep',	'Fuzzy Grep']	,
    \ 'h' : [':Telescope help_tags',	'Fuzzy Help']	,
    \ }
" File related
let g:which_key_map.f = {
    \ 'name' : 'Files'				,
    \ 's' : [':w',		'Save']		,
    \ }
" Quit related
let g:which_key_map.q = {
    \ 'name' : 'Quit'				,
    \ 's' : [':wq!',	'Saving']		,
    \ 'q' : [':q!',	'Not Saving']	,
    \ }
"Git
let g:which_key_map.g = {
    \ 'name' : 'Git',
    \ 'g' : [':Git' , 'Git'],
    \ 'p' : [':Git push origin master' , 'Push origin master'],
    \ }
" Open Commands related
let g:which_key_map.o = {
    \ 'name' : 'Open'				,
    \ 't' : [':te',	'Terminal']		,
    \ 'n' : [':NERDTreeToggle',	'NERDTree']	,
    \ 'l' : [':Lex',  'Lex'],
    \ 's' : [':Startify', 'Startify'],
    \ 'r' : [':RnvimrToggle', 'Ranger'],
    \ 'f' : [':NnnPicker', 'NNN']
    \ }
" EOF
