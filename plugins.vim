" Plugin Installation file for NeoVim
" Author: Javier de Marco
call plug#begin('~/.config/nvim/plugged')
" Interface
Plug 'itchyny/lightline.vim' " Statusbar *
Plug 'sonph/onehalf', {'rtp': 'vim/'} " *
Plug 'morhetz/gruvbox'
Plug 'ryanoasis/vim-devicons' " Icons *
Plug 'mhinz/vim-startify' " Welcome screen
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'bagrat/vim-buffet' " Tabs for buffers
Plug 'yggdroot/indentline' " Indentation Guides
" Markdown
Plug 'tpope/vim-markdown'
Plug 'instant-markdown/vim-instant-markdown', {'for': 'markdown'}
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } } " Markdown
" Linter
Plug 'w0rp/ale' " Sintax error highlight
" Fuzzy Finder
Plug 'nvim-lua/plenary.nvim' " Telescope dependenzy
Plug 'nvim-telescope/telescope.nvim'
" Git
Plug 'Xuyuanp/nerdtree-git-plugin' " Nerdtree git icons
Plug 'tpope/vim-fugitive' " Git Wrapper
" File Tree
Plug 'preservim/nerdtree' " File Explorer
Plug 'kevinhwang91/rnvimr', {'do': 'make sync'} " Ranger in floating window
" Project manger
Plug 'tpope/vim-projectionist' " Project manager
" Key Mapping
Plug 'liuchengxu/vim-which-key' " Mapping utility Compilers
" Shell commands shortcuts
Plug 'tpope/vim-eunuch'
" Editing tools
Plug 'tpope/vim-surround' " Add surround operator (s)
Plug 'tpope/vim-commentary' " Adds comment operator (gc)
Plug 'jiangmiao/auto-pairs' " Always insert in pairs
Plug 'luochen1990/rainbow' " Individual coloring for surrounds
Plug 'christoomey/vim-sort-motion' " Sort
Plug 'michaeljsmith/vim-indent-object' " Adds indent as text object
Plug 'tpope/vim-repeat'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'tpope/vim-commentary'
" Autocomplete
Plug 'neoclide/coc.nvim', {'branch': 'release'}
call plug#end()
" Theme
"colorscheme onehalfdark
colorscheme gruvbox
" Ale config
let g:ale_sign_column_always = 1
" Which Key
call which_key#register('<Space>', "g:which_key_map")
" NERDTree
let g:NERDSpaceDelims = 1
let g:NERDCommentEmptyLines = 1
let g:NERDTrimTrailingWhitespace = 1
let NERDTreeShowHidden = 1
let NERDTreeMinimalUI = 1
let g:NERDTreeFileExtensionHighlightFullName = 1
let g:NERDTreeExactMatchHighlightFullName = 1
let g:NERDTreePatternMatchHighlightFullName = 1
" Rainbow parenthesis
let g:rainbow_active = 1
" Lightline
let g:lightline = {
    \ 'colorscheme': 'gruvbox',
    \ 'active': {
    \   'left': [ [ 'mode', 'paste' ],
    \             [ 'gitbranch', 'readonly', 'filename', 'modified'] ]
    \ },
    \ 'component_function': {
    \   'gitbranch': 'FugitiveHead',
    \ },
    \ }
" Startify
" let g:startify_bookmarks
let g:startify_session_dir = '~/Documents/VimSessions'
let g:startify_custom_header =
            \ startify#pad(split(system('figlet -c "Zero Vim"'), '\n'))
let g:startify_lists = [
        \ { 'type': 'files',     'header': ['   MRU']            },
        \ { 'type': 'sessions',  'header': ['   Sessions']       },
        \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
        \ ]
" Markdown config
let g:mkdp_browser = 'vivaldi'
" Tabs
let g:buffet_show_index = 1
let g:buffet_use_devicons = 1	
function! g:BuffetSetCustomColors()
  hi! BuffetCurrentBuffer cterm=NONE ctermbg=5 ctermfg=8 guibg=#282c34 guifg=#dcdfe4
endfunction
" Ranger
let g:rnvimr_ex_enable = 1
