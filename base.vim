" Author: Javier de Marco
" Config file for NeoVim 
" Configuration
" Leader Key
" Pyhton provider
let $PAGER=''
let g:mapleader = "\<Space>"
let g:python3_host_prog  = '/usr/bin/python'
let g:rehash256=1
set autoindent
set backspace=2
set clipboard+=unnamedplus
set cmdheight=2
set expandtab
set guifont="monospace:13"
set guioptions-=L
set guioptions-=T
set guioptions-=m
set guioptions-=r
set hidden
set hlsearch
set incsearch
set laststatus=2
set mouse=nicr
set nobackup
set nowritebackup
set nu rnu
set shiftwidth=4
set showcmd
set signcolumn=yes
set smarttab
set nospell
set spelllang=en_us,es_es
set splitright splitbelow 
set t_co=256
set tabstop=4
set textwidth=100
set timeoutlen=300
set updatetime=300
set wildmenu
set termguicolors
set shortmess+=c
syntax on
